#include <stdlib.h>
#include <time.h>
#include <ncurses.h>

#define CURSOR '@'
#define FLAG 'F'
#define COVERED '-'
#define BORDER '#'

#define CURSORCOLOR 0
#define FLAGCOLOR 1
#define COVEREDCOLOR 2
#define BOMBCOLOR 3

#define BOARDHEIGHT 16
#define BOARDWIDTH 64

struct TextBox {
	char *title;
	char *msg;
	int x, y, w, h;
};

// Used for both cursor and mines
struct Position {
	int x, y; // x/y pos
	int flagged; // If the player flagged it
	int bomb; // If the position is a bomb
	int surrounding; // Amount of bombs surrounding this position
	int uncovered;
};

struct Position board[BOARDWIDTH * BOARDHEIGHT];

struct Position cursorpos = { .x = 0, .y = 0 };

void init_board_bombs() {
	mvprintw(0, BOARDWIDTH + 2, "%d %d", cursorpos.x, cursorpos.y);
	getch();
	for(int i = 0; i < BOARDWIDTH * BOARDHEIGHT; i++) {
		struct Position *cur = &board[i];
		if((rand() % 100) > 80)
			cur->bomb = 1;

		if(cur->x == cursorpos.x && cur->y == cursorpos.y) {
			mvaddch(cur->y, cur->x, 'a');
			getch();
			cur->bomb = 0;
		}
	}

	int cy = -1;
	for(int i = 0; i < BOARDWIDTH * BOARDHEIGHT; i++) {
		struct Position *cur = &board[i];
		if(cur->bomb) continue;

		int cx = i % BOARDWIDTH;
		if(cx == 0) cy++;

		for(int y = cy-1; y < cy+2; y++)
			for(int x = cx-1; x < cx+2; x++) {
				if(y == cy && x == cx) continue;
				if(y < 0 || x < 0
				|| y >= BOARDHEIGHT || x >= BOARDWIDTH) continue;
				if(board[x + y * BOARDWIDTH].bomb) cur->surrounding++;
		}
	}
}

void init_board() {
	int y = -1;
	for(int i = 0; i < BOARDWIDTH * BOARDHEIGHT; i++) {
		struct Position *cur = &board[i];
		cur->x = i % BOARDWIDTH;
		if(cur->x == 0) y++;
		cur->y = y;

		cur->surrounding = 0;
		cur->flagged = 0;
		cur->uncovered = 0;
		cur->bomb = 0;

	}
}

void draw_board(int show) {
	for(int y = 0; y < BOARDHEIGHT+1; y++)
		mvaddch(y, 0, BORDER);
	for(int x = 0; x < BOARDWIDTH+1; x++)
		mvaddch(0, x, BORDER);

	for(int y = 0; y < BOARDHEIGHT+2; y++)
		mvaddch(y, BOARDWIDTH+1, BORDER);
	for(int x = 0; x < BOARDWIDTH+1; x++)
		mvaddch(BOARDHEIGHT+1, x, BORDER);

	for(int y = 0; y < BOARDHEIGHT; y++)
		for(int x = 0; x < BOARDWIDTH; x++) {
			struct Position *cur = &board[x + y * BOARDWIDTH];
			if(cur->uncovered) {
				mvprintw(y+1, x+1, "%d", cur->surrounding);
			} else if(cur->flagged) {
				attron(COLOR_PAIR(FLAGCOLOR));
				mvaddch(y+1, x+1, FLAG);
				attroff(COLOR_PAIR(FLAGCOLOR));
			} else if(show && cur->bomb) {
				attron(COLOR_PAIR(BOMBCOLOR));
				mvaddch(y+1, x+1, 'B');
				attroff(COLOR_PAIR(BOMBCOLOR));
			} else {
				attron(COLOR_PAIR(COVEREDCOLOR));
				mvaddch(y+1, x+1, COVERED);
				attroff(COLOR_PAIR(COVEREDCOLOR));
			}
		}
	refresh();
}

void start_ncurses() {
	initscr();
	noecho();
	cbreak();
	curs_set(0);
}

void end_ncurses() {
	curs_set(1);
	nocbreak();
	echo();
	endwin();
}

void draw_textbox(struct TextBox *tb) {
	for(int y = tb->y; y < tb->y+tb->h; y++)
		for(int x = tb->x; x < tb->x+tb->w; x++)
			mvprintw(y, x, " ");

	mvprintw(tb->y+1, tb->x+1, tb->msg);

	for(int y = tb->y; y < tb->y+tb->h; y++)
		mvaddch(y, tb->x, BORDER);
	for(int x = tb->x; x < tb->x+tb->w; x++)
		mvaddch(tb->y, x, BORDER);
	for(int y = tb->y; y < tb->y+tb->h; y++)
		mvaddch(y, tb->x+tb->w, BORDER);
	for(int x = tb->x; x < tb->x+tb->w+1; x++) // +1 for last corner
		mvaddch(tb->y+tb->h, x, BORDER);

	mvprintw(tb->y, tb->x+1, tb->title);

	refresh();
}

int quit() {
	struct TextBox tb = {.title = "Quit?", .msg = "Are you sure\n \
you want to quit? [y/N]", .x = 0, .y = 0, .w = 33, .h = 3};

	draw_textbox(&tb);
	switch(getch()) {
		case 'y':
		case 'Y':
			return 1;
		default:
			return 0;
	}
}

void help() {
	struct TextBox tb = {.title = "Help", 
		.msg = "[w/k] Move cursor up\n \
[a/h] Move cursor left\n \
[s/j] Move cursor down\n \
[d/l] Move cursor right\n \
[f] Flag a plot\n \
[ ] Dig a plot\n", .x = 0, .y = 0, .w = 33, .h = 7};

	draw_textbox(&tb);
}

void results(int win) {
	char *message;
	if(win) message = "You win!";
	else message = "You lose!";
	struct TextBox tb = {.title = "Results", 
		.msg = message, .x = 0, .y = 0, .w = 33, .h = 7};

	draw_board(1);
	draw_textbox(&tb);
	getch();
}

void finish() {
	end_ncurses();
	exit(0);
}

int main() {
	srand(time(0));
	start_ncurses();
	init_board();

	start_color();
	init_pair(CURSORCOLOR, COLOR_BLACK, COLOR_WHITE);
	init_pair(FLAGCOLOR, COLOR_RED, COLOR_BLACK);
	init_pair(COVEREDCOLOR, COLOR_BLACK, COLOR_GREEN);
	init_pair(BOMBCOLOR, COLOR_BLACK, COLOR_WHITE);
	init_pair(16, COLOR_WHITE, COLOR_BLACK);
	bkgd(COLOR_PAIR(16));

	struct Position *cur;

	int first_dig = 1;
	int alive = 1;
	while(alive) {
		// Cursor snap
		if(cursorpos.y < 1) cursorpos.y = 1;
		else if(cursorpos.y > BOARDHEIGHT) cursorpos.y = BOARDHEIGHT;
		if(cursorpos.x < 1) cursorpos.x = 1;
		else if(cursorpos.x > BOARDWIDTH) cursorpos.x = BOARDWIDTH;

		clear();

		draw_board(1);
		attron(COLOR_PAIR(CURSORCOLOR));
		mvaddch(cursorpos.y, cursorpos.x, CURSOR);
		attroff(COLOR_PAIR(CURSORCOLOR));

		refresh();
		switch(getch()) {
			case 'q':
				if(quit()) finish();
				break;

			case '?':
				help();
				getch();
				break;

			case 'w':
			case 'k':
				cursorpos.y--;
				break;

			case 'a':
			case 'h':
				cursorpos.x--;
				break;

			case 's':
			case 'j':
				cursorpos.y++;
				break;

			case 'd':
			case 'l':
				cursorpos.x++;
				break;

			case 'f':
				cur = &board[(cursorpos.x-1) + (cursorpos.y-1) * BOARDWIDTH];
				if(cur == NULL) break;
				cur->flagged = !cur->flagged;
				cur = NULL;
				break;
			case ' ':
				if(first_dig) {
					init_board_bombs();
					first_dig = 0;
				}
				cur = &board[(cursorpos.x-1) + (cursorpos.y-1) * BOARDWIDTH];
				if(cur == NULL) break;
				if(!cur->flagged) {
					cur->uncovered = 1;
					if(cur->bomb) {
						results(0);
						finish();
					}
				}
				cur = NULL;
				break;
		}
	}
	getch();
}
